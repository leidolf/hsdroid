package com.brockoli.android.hsdroid.utils;

import java.util.ArrayList;

import org.joda.time.LocalDate;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.brockoli.android.hsdroid.provider.HSDroidContract;


public class DBUtils {
	private static final String TAG = DBUtils.class.getSimpleName();

	/*********************************************************************
	 * 
	 * Teams table operations
	 *
	 *********************************************************************/
	public static void insertOrUpdateTeam(Context ctx, String team) {
		ContentValues cv = new ContentValues();
		cv.put(HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME, team);
		if (teamExists(ctx, team)) {
			String where = HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME + " = ?";
			String[] selectionArgs = { team };
			try {
				ctx.getContentResolver().update(HSDroidContract.Teams.CONTENT_URI, cv, where, selectionArgs);
				Log.d(TAG, "Updated Team: " + team);
			} catch (SQLException ex) {
				Log.e(TAG, "SQLException: " + ex);
			}
		} else {
			try {
				ctx.getContentResolver().insert(HSDroidContract.Teams.CONTENT_URI, cv);
				Log.d(TAG, "Inserted Show: " + team);
			} catch (SQLException ex) {
				Log.e(TAG, "SQLException: " + ex);
			}			
		}
	}


	public static Boolean teamExists(Context ctx, String team) {
		Boolean exists = false;
		String[] projection = { HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME };
		String[] args = { team };
		String where = HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME + " = ?";
		CursorLoader cL = new CursorLoader(ctx, HSDroidContract.Teams.CONTENT_URI, projection, where, args,
				HSDroidContract.Teams.DEFAULT_SORT_ORDER);
		Cursor cursor = cL.loadInBackground();
		if ((cursor != null) && (cursor.getCount() > 0)) {
			exists = true;
		}

		cursor.close();
		return exists;
	}

	public static int deleteTeam(Context ctx, String team) {
		String where = HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME + " = ?";
		String[] args = { team };
		return ctx.getContentResolver().delete(HSDroidContract.Teams.CONTENT_URI, where, args);
	}

	public static ArrayList<String> getFavoriteTeams(Context ctx) {
		ArrayList<String> teams = new ArrayList<String>();
		String[] projection = { HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME };
		CursorLoader cL = new CursorLoader(ctx, HSDroidContract.Teams.CONTENT_URI, projection, null, null,
				HSDroidContract.Teams.DEFAULT_SORT_ORDER);
		Cursor cursor = cL.loadInBackground();
		if ((cursor != null) && (cursor.getCount() > 0)) {
			cursor.moveToFirst();
			while (cursor.isAfterLast() == false) {
				teams.add(cursor.getString(cursor.getColumnIndexOrThrow(HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME)));
				cursor.moveToNext();
			}
		}
		return teams;
	}

	/*********************************************************************
	 * 
	 * Notifications table operations
	 *
	 *********************************************************************/
	public static Boolean notificationExists(Context ctx, String id) {
		Boolean exists = false;
		String[] projection = { HSDroidContract.Notifications.COLUMN_NAME_NOTIFICATION_ID };
		String[] args = { id };
		String where = HSDroidContract.Notifications.COLUMN_NAME_NOTIFICATION_ID + " = ?";
		CursorLoader cL = new CursorLoader(ctx, HSDroidContract.Notifications.CONTENT_URI, projection, where, args,
				HSDroidContract.Notifications.DEFAULT_SORT_ORDER);
		Cursor cursor = cL.loadInBackground();
		if ((cursor != null) && (cursor.getCount() > 0)) {
			exists = true;
		}

		cursor.close();
		return exists;
	}

	public static void insertNotification(Context ctx, String id) {
		ContentValues cv = new ContentValues();
		cv.put(HSDroidContract.Notifications.COLUMN_NAME_NOTIFICATION_ID, id);
		try {
			ctx.getContentResolver().insert(HSDroidContract.Notifications.CONTENT_URI, cv);
			Log.d(TAG, "Inserted Notification: " + id);
		} catch (SQLException ex) {
			Log.e(TAG, "SQLException: " + ex);
		}			
	}

	// Delete any notifications that aren't from today
	public static int deleteOldNotifications(Context ctx) {
		LocalDate today = new LocalDate();
		long millis = today.toDateTimeAtStartOfDay().getMillis();
		String where = HSDroidContract.Notifications.COLUMN_NAME_CREATE_DATE + " < ?";
		String[] args = { Long.toString(millis) };
		return ctx.getContentResolver().delete(HSDroidContract.Notifications.CONTENT_URI, where, args);
		
	}
}
